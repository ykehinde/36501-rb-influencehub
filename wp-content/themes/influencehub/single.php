<?php
/**
 * The Template for displaying all single posts
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header' ) ); ?>

		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<article>

			<div class="content">
                <div class="header">
                    <h1><?php the_title(); ?></h1>
                </div>

                <div class="meta">
                    <p>
                         <?php the_tags('<span>Tags: </span>', ', ', ''); ?></i>
                    </p>
                </div>

                <div class="post">
                <?php the_content(); ?>

                    <div id="disqus_thread"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
        var disqus_shortname = 'rbinfluencehub'; // required: replace example with your forum shortname

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
    
                </div>

        </div>
        <footer class="footer clearfix" role="contentinfo">
            <p class="old"><?php next_post_link('%link', '<span class="icon-arrow-right">'); ?></p>
            <p class="new"><?php previous_post_link('%link', '<span class="icon-arrow-left">'); ?></p>
            <div class="clearfix"></div>
        </footer>
		</article>

		<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-footer' ) ); ?>
