<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header' ) ); ?>

            <div class="blogTable clearfix">
                <div class="gutter-sizer"></div>
                <div class="grid-sizer"></div>
                <ul class="blogList unstyled clearfix">
                    <li class="blogList-item welcome">
                        <figure>
                            <figcaption>
                                <div>
                                    <p>Welcome to the Influence Hub, RB's very own source of all things creative in the advertising and marketing industry. Here you’ll find everything from clever uses of technology to quirky use of media spaces to innovative digital campaigns. No need to trawl the Internet, the good stuff is all here.</p>
                                </div>
                            </figcaption>
                        </figure>
                    </li>

                    <li class="blogList-item tag-cloud">
                        <figure>
                            <figcaption>
                                <p>
                                    <?php wp_tag_cloud ($args = array(
                                        'smallest'                  => 8, 
                                        'largest'                   => 22,
                                        'unit'                      => 'pt', 
                                        'number'                    => 45,  
                                        'format'                    => 'flat',
                                        'separator'                 => "\n",
                                        'orderby'                   => 'name', 
                                        'order'                     => 'ASC',
                                        'exclude'                   => null, 
                                        'include'                   => null, 
                                        'topic_count_text_callback' => default_topic_count_text,
                                        'link'                      => 'view', 
                                        'taxonomy'                  => 'post_tag', 
                                        'echo'                      => true,
                                        'child_of'                  => null, // see Note!
                                    )); ?>
                                </p>
                            </figcaption>
                        </figure>
                    </li>
                </ul>
            </div>


		<?php if ( have_posts() ): ?>
        <div class="blogTable clearfix">
        <div class="gutter-sizer"></div>
        <div class="grid-sizer"></div>
            <ul class="blogList unstyled clearfix">


		<?php while ( have_posts() ) : the_post(); ?>
                <li class="blogList-item blogList-large">
                    <a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">
                        <figure class="effeckt-caption">
                            <?php
                                $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'your_thumb_handle' ); ?>
<img src="<?php echo $thumbnail['0']; ?>" />

                            <figcaption>
                                <div class="effeckt-figcaption-wrap">
                                    <h3><?php the_title(); ?></h3>
                                </div>
                            </figcaption>
                        </figure>
                    </a>
                </li>
		<?php endwhile; ?>
		</ul>
		<?php else: ?>
		<h2>No posts to display</h2>
		<?php endif; ?>
        </div>
        <?php if ($wp_query->max_num_pages > 1) : ?>
            <footer class="footer clearfix" role="contentinfo"></span>
                <p class="new"><?php previous_posts_link(__( '<span class="icon-arrow-left">', 'roots' )); ?></p>
                <p class="old"><?php next_posts_link(__( '<span class="icon-arrow-right">', 'roots' )); ?></p>
                <div class="clearfix"></div>
            </footer>
        <?php endif; ?>



<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-footer' ) ); ?>
