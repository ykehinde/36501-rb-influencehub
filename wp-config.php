<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
switch(true)
{
	case (strpos($_SERVER['SERVER_NAME'], '.dev') || strpos($_SERVER['SERVER_NAME'], '.local') ) :
		define('WP_DEBUG', true);
		error_reporting(E_ALL|E_STRICT);
		ini_set('display_errors', 1);

		define('DB_NAME', 'rb-influencehub');
		define('DB_USER', 'root');
		define('DB_PASSWORD', 'password');
		define('DB_HOST', 'localhost');
	break;
	default :
		// turn off error reporting in production
		define('WP_DEBUG', false);
		error_reporting(0);
		@ini_set('display_errors', 0);

		// set your production database values
		define('DB_NAME', 'rbinfluencehub');
		define('DB_USER', 'rbinfluencehub');
		define('DB_PASSWORD', 'F5NLQhq7=damGvP');
		define('DB_HOST', 'localhost');
	break;
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Se><9jL8*X`vx$w24oiwr`q9O+D%M9>QJCAn}0(-mF_g[nb:.g-C^-EP`c%MIv2l');
define('SECURE_AUTH_KEY',  'QeS!{tw`QA9giZH)]Vi51adrR0S:r0$c0G)=tY^/a5H05VHCxEtAf:+!ADs%@?C_');
define('LOGGED_IN_KEY',    '@YG!RM64w>lw[l`fV1`%r6ZfU!|C_P6fx<hsmXP>02@h()h.gb)O:|C<46;wx&F+');
define('NONCE_KEY',        '5w89qv=tJ.g4{-*l{~N>Ogpek]%gj&|](&ms$+:a&$`#*_f~|d+Tqk,A9Fm[k]|N');
define('AUTH_SALT',        'g>ir_= !ab(Afn;SHDY>bzE)64uhI*io3VZY6[f1pz-_^V-Qz}lNl8&O54i*%P/U');
define('SECURE_AUTH_SALT', '4 E+9e*Fp,f?%/0-4KU|Cv@M{4wYx=J&Jp()J[)Z11*Y-k/=rL&>iV+&C)dgq~f5');
define('LOGGED_IN_SALT',   'Hz+pbh$%MmCi2(|]Dw, p%IE,zmc.Zi%~k+V#B~H|If^{,evX (Ow>,K8YDdI[,q');
define('NONCE_SALT',       'e_;+5$|k3<7=d!1<*6WjYy+<-d@ezhL,^ozd;;f+|!2cFu{+s&SzS[a}Gu|y|KwL');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
