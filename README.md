# Diageo's influence hub #

Wordpress based blog built primarily for reckitt benckiser employees whilst being managed at tmw.

### Languages ###

* HTML
* CSS
* PHP

### How do I get set up? ###

* Download/Pull the repo
* Run $npm i
* Check the wp-config file and set up your database on your local server
* Run the site using a .dev or .local domain as the config file will automatically know it's a development server

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Developer - Yemi Kehinde
* Developer - Fabian Andrade
* Tech Lead - Ciaran Park
* Tech Lead - Zander Martineau
* Designer - Luke Clark
* Oversight - Justin Moody
* .Net Developer - Ed Acheampong